# Maze Navigator

Walk through a maze, loaded from an image.
To generate mazes, use Mazer.


## Compile

Requires Java 8 and Gradle.
Compile and run with:

```
gradle build
java -jar build/libs/maze-navigator.jar
```
