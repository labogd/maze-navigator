package maze.navigator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;

public class MazeLoader {

    public static Maze[][] load(Path source) {
        try {
            final WritableImage image = SwingFXUtils.toFXImage(ImageIO.read(source.toFile()), null);
            final PixelReader reader = image.getPixelReader();
            final Maze[][] maze = new Maze[(int) image.getHeight()][];
            Arrays.parallelSetAll(maze, row -> {
                final Maze[] mazeRow = new Maze[(int) image.getWidth()];
                Arrays.parallelSetAll(mazeRow, column -> cellType(reader.getColor(column, row)));
                return mazeRow;
            });
            return maze;
        } catch (IOException exception) {
            throw new IllegalStateException(exception.getMessage(), exception);
        }
    }

    private static Maze cellType(Color color) {
        return color.equals(Color.WHITE) ? Maze.AISLE : Maze.WALL;
    }
}
