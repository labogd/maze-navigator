package maze.navigator;

import java.util.stream.IntStream;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class MazeView extends Group {

    private final Maze[][] maze;
    private final Shape character;
    private final Controls controls;
    private final ObservableIntegerValue size;
    private final ObservableDoubleValue speed;

    public MazeView(Maze[][] maze, Controls controls, ObservableIntegerValue size, ObservableDoubleValue speed, Rectangle view) {
        this.maze = maze;
        this.controls = controls;
        this.size = size;
        this.speed = speed;
        character = new Circle(10, 10, 10, Color.RED);
        IntStream.range(0, maze.length).forEach(row -> {
            IntStream.range(0, maze[row].length).forEach(column -> {
                getChildren().add(createCellAt(row, column));
            });
        });
        getChildren().add(character);
        size.addListener((observable, oldValue, newValue) -> {
            character.setLayoutX(character.getLayoutX() * newValue.doubleValue() / oldValue.doubleValue());
            character.setLayoutY(character.getLayoutY() * newValue.doubleValue() / oldValue.doubleValue());
        });
        final DoubleBinding semiWidth = view.widthProperty().divide(2);
        final DoubleBinding semiHeight = view.heightProperty().divide(2);
        view.xProperty().bind(character.layoutXProperty().subtract(semiWidth));
        view.yProperty().bind(character.layoutYProperty().subtract(semiHeight));
        layoutXProperty().bind(character.layoutXProperty().negate().add(semiWidth));
        layoutYProperty().bind(character.layoutYProperty().negate().add(semiHeight));
        setClip(view);
    }

    private Rectangle createCellAt(int row, int column) {
        final Rectangle cell = new Rectangle();
        cell.widthProperty().bind(size);
        cell.heightProperty().bind(size);
        cell.xProperty().bind(new SimpleDoubleProperty(column).multiply(size));
        cell.yProperty().bind(new SimpleDoubleProperty(row).multiply(size));
        cell.setFill(maze[row][column] == Maze.AISLE ? Color.WHITE : Color.BLACK);
        return cell;
    }

    public int width() {
        return maze.length == 0 ? 0 : maze[0].length * size.get();
    }

    public int height() {
        return maze.length * size.get();
    }

    public void update() {
        double x = character.getLayoutX() + 10;
        double y = character.getLayoutY() + 10;
        if (controls.up()) {
            y -= speed.get();
            if (y - 10 < 0) {
                y = 10;
            }
            if (maze[(int) ((y - 10) / size.get())][(int) (x / size.get())] == Maze.WALL) {
                y = ((((int) ((y - 10) / size.get())) + 1) * size.get()) + 10;
            }
        }
        if (controls.down()) {
            y += speed.get();
            if (y + 10 >= height()) {
                y = height() - 10;
            }
            if (maze[(int) ((y + 10) / size.get())][(int) (x / size.get())] == Maze.WALL) {
                y = ((((int) ((y + 10) / size.get()))) * size.get()) - 10;
            }
        }
        if (controls.left()) {
            x -= speed.get();
            if (x - 10 < 0) {
                x = 10;
            }
            if (maze[(int) (y / size.get())][(int) ((x - 10) / size.get())] == Maze.WALL) {
                x = ((((int) ((x - 10) / size.get())) + 1) * size.get()) + 10;
            }
        }
        if (controls.right()) {
            x += speed.get();
            if (x + 10 >= width()) {
                x = width() - 10;
            }
            if (maze[(int) (y / size.get())][(int) ((x + 10) / size.get())] == Maze.WALL) {
                x = ((((int) ((x + 10) / size.get()))) * size.get()) - 10;
            }
        }
        character.setLayoutX(x - 10);
        character.setLayoutY(y - 10);
    }
}
