package maze.navigator;

import javafx.scene.input.KeyEvent;

public class Controls {

    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;

    public void handleKeyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case W:
                up = true;
                return;
            case S:
                down = true;
                return;
            case A:
                left = true;
                return;
            case D:
                right = true;
        }
    }

    public void handleKeyReleased(KeyEvent event) {
        switch (event.getCode()) {
            case W:
                up = false;
                return;
            case S:
                down = false;
                return;
            case A:
                left = false;
                return;
            case D:
                right = false;
        }
    }

    public boolean up() {
        return up;
    }

    public boolean down() {
        return down;
    }

    public boolean left() {
        return left;
    }

    public boolean right() {
        return right;
    }
}
