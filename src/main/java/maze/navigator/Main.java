package maze.navigator;

import java.io.File;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final Scene scene = new Scene(new Group(), Color.BLACK);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Maze Navigator");
        final File mazeImage = selectMazeImage(scene);
        if (mazeImage == null) {
            primaryStage.close();
            Platform.exit();
        } else {
            final Controls controls = new Controls();
            final Maze[][] maze = MazeLoader.load(mazeImage.toPath());
            final MazeNavigator mazeNavigator = new MazeNavigator(maze, controls, 320, 100);
            scene.setRoot(mazeNavigator);
            scene.setOnKeyPressed(controls::handleKeyPressed);
            scene.setOnKeyReleased(controls::handleKeyReleased);
            primaryStage.show();
        }
    }

    private static File selectMazeImage(Scene scene) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load maze");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        return fileChooser.showOpenDialog(scene.getWindow());
    }
}
