package maze.navigator;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.geometry.Insets;
import javafx.scene.control.Slider;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class MazeNavigator extends BorderPane {

    private final MazeView maze;
    private final Rectangle view;
    private final Slider speed;
    private final Slider size;

    public MazeNavigator(Maze[][] maze, Controls controls, int size, double speed) {
        this.speed = new Slider(0, speed, speed);
        this.size = new Slider(20, size, size);
        view = new Rectangle(800, 600);
        final IntegerBinding sizeBinding = Bindings.createIntegerBinding(() -> (int) this.size.getValue(), this.size.valueProperty());
        this.maze = new MazeView(maze, controls, sizeBinding, this.speed.valueProperty(), view);
        initComponents();
    }

    private void initComponents() {
        setPadding(new Insets(10));
        setBackground(new Background(new BackgroundFill(Color.DARKSLATEGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        setLeft(new VBox(10,
                speedPane(),
                sizePane()
        ));
        setMargin(getLeft(), new Insets(0, 10, 0, 0));
        setCenter(mazePane());
        animate(maze);
    }

    private TitledPane speedPane() {
        speed.setShowTickMarks(true);
        speed.setShowTickLabels(true);
        return new TitledPane("speed", speed);
    }

    private TitledPane sizePane() {
        size.setBlockIncrement(1);
        size.setShowTickMarks(true);
        size.setShowTickLabels(true);
        return new TitledPane("size", size);
    }

    private Pane mazePane() {
        final Pane container = new Pane(maze);
        container.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        container.maxWidthProperty().bind(view.widthProperty());
        container.maxHeightProperty().bind(view.heightProperty());
        return container;
    }

    private static void animate(MazeView mazeView) {
        Platform.runLater(() -> {
            final Timeline timer = new Timeline(new KeyFrame(Duration.millis(40), tick -> mazeView.update()));
            timer.setCycleCount(Timeline.INDEFINITE);
            timer.play();
        });
    }
}
